//
//  ViewController.swift
//  dz4
//
//  Created by Максим Бойко on 3/17/19.
//  Copyright © 2019 Максим Бойко. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        dzPart1(name: "Максим")
//        dzPart2(name: "Валериевич")
//        dzPart3(name: "Максим", surName: "Валериевич")
//        dzPart4(str: "Привет меня зовут Максим")
//        dzPart5(str: "1234567999") // очень тяжелое для меня получилось
//        dzPart7(count: 15)
//         dzPart8(name: "YAZZ")
        dzPart9(array: ["lada","sedan","vlad","baklazan"])
    
    }

    func dzPart1(name: String) {
        let myName = name
        print("\(myName) \(myName.count) букв")
    }
    
    func dzPart2(name: String) {
        let mySurName = name
        
        if mySurName.hasSuffix("ич") || mySurName.hasSuffix("на") {
            print("Фамилия \(mySurName) имеет нужный суфикс ")
            
        }
    }
    
    func dzPart3(name: String,surName : String) {
        let login = name + " " + surName
        
        print(login)
    }
    
    func dzPart4(str: String) {
        let string1 = str
        var newStr = String()
        for i in string1 {
            newStr.insert(i, at: newStr.startIndex)
        }
        print(newStr)
    }
    
    func dzPart5(str: String) {
        var counter = 1
        var string1 = str
        var finish = String ()
        var newStr = String()
        for i in string1 {
            newStr.insert(i, at: newStr.startIndex) //развернули число и записали в newStr
        }
        for i in newStr {
            
            finish.insert(i, at: finish.startIndex) // разворачиваем обратно и добавляем запятаую
           if counter % 3 == 0 && counter < newStr.count {
            
                finish.insert(",", at: finish.startIndex)
        }
           counter += 1
        }
        print(finish)
    }
    
    func dzPart7(count: Int) {
        var someInts = [Int]()
        let counter = count
        for _ in 0...counter {
            someInts.append(Int.random(in: 0..<10)) //генерируем масив
        }
       print(someInts)
        
        
        for i in 0...(someInts.count-1) {
            for j in 0...(someInts.count-1) {
                if i != j {
                    if someInts[i] < someInts[j]{ //сортируем
                  let x = someInts[i]
                        someInts[i] = someInts[j]
                        someInts[j] = x
                    }
                }
            }
        }
        
        print(someInts)
        for _ in 0...10 {
            for i in 0...(someInts.count - 2) {
                if someInts[i]==someInts[i+1]{
                    someInts.remove(at: i+1) //удаляем повторы (максимум 10 повторов . можно изменить в верзнец цыкле)
                    break
            }
            }
        }
        print(someInts)
        
    }
    
    func dzPart8(name: String) {
     
        var translite: [String: String] = ["a": "а", "p": "п", "k": "к","Y":"Я","A":"A","Z":"З"]
        for i in name {
            print(translite["\(i)"]! , terminator: "")
        }
    }
    
    func dzPart9(array: [String]) {
        
        var newArray = [String]()
        
        for i in 0...(array.count - 1) {
           
            var counter = 0
            for j in array[i]{
                if counter == 1 && j=="a" {   //если счетчик =1 и буква равна а то вместе будет да
                    newArray.append(array[i])
                }
                
                if j == "d" {
                    counter = counter + 1   //если есть д то добавляем к счетчику 1 если                  нет то обнуляем
                }
                else {
                    counter = 0
                }
                
                
                }
        }
        print(newArray)
        
    }
    
    
        }
        

    
    


